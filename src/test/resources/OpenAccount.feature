Feature: Open Account in the system
  After customers are added in the system
  We need the Open Account for all customers in the system

  Scenario Outline: Open account for customer successfully
    Given I open the Open account website <website>
    Then I verify that the correct Open account form
    When I select the customer name <firstname> <lastname>, select the currency <currency>
    And I click Process button
    Then I see the Open Acount successful dialog is appeared
    And I verify the Account is added successfully
    Examples:
      | website                                                                            | firstname | lastname | currency |
      | http://www.globalsqa.com/angularJs-protractor/BankingProject/#/manager/openAccount | Hung      | Truong   | Dollar   |