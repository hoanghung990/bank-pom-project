package actions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import page.CustomersPage;
import page.OpenAccountPage;
import steps.Setup;

import java.util.List;

public class CustomersAction {
    public static void clickCustomer() throws InterruptedException {
        CustomersPage.customersBtn.click();
        Thread.sleep(3000);
    }

    public static void verifyAccount(WebDriver driver, String firstname, String lastname) {
        WebElement table = driver.findElements(By.tagName("table")).get(0);
        WebElement tbodytablee = table.findElement(By.tagName("tbody"));
        List<WebElement> trlistt = tbodytablee.findElements(By.tagName("tr"));
        Boolean checkExisted = false;
        for (WebElement tr : trlistt) {
            //lay dong dau tien va xoa dong dau tien do
            //List<WebElement> tdlistt = trlistt.get(0).findElements(By.tagName("td"));
            List<WebElement> tdlistt = tr.findElements(By.tagName("td"));
            if (tdlistt != null && tdlistt.size() > 0) {
                String firstn = tdlistt.get(0).getText();
                System.out.println(firstn);
                System.out.println(firstname);
                if (firstn.toLowerCase().equals(firstname.toLowerCase())) {
                    checkExisted = true;
                }
            }
        }
        Assert.assertEquals(checkExisted, true);
    }
    public static void scrollTable()throws InterruptedException{
        List<WebElement> webElementList = Setup.driver.findElement(By.tagName("table")).findElement(By.tagName("tbody")).findElements(By.tagName("tr"));
        List<WebElement> listtd = webElementList.get(0).findElements(By.tagName("td"));
        WebElement td = listtd.get(listtd.size()-1);
        JavascriptExecutor js = (JavascriptExecutor) Setup.driver; js.executeScript("arguments[0].scrollIntoView(true);",td);
        Thread.sleep(10000);
    }
}
