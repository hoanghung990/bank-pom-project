package actions;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import page.AddCustomerPage;

import java.util.HashMap;

public class AddCustomerAction {
    public static void Execute(WebDriver driver, HashMap<String, String> map) throws Exception {
        AddCustomerPage.firstname.clear();
        AddCustomerPage.firstname.sendKeys(map.get("firstnameparameter"));
        Thread.sleep(1500);

        AddCustomerPage.lastname.clear();
        AddCustomerPage.lastname.sendKeys(map.get("lastnameparameter"));
        Thread.sleep(1500);

        AddCustomerPage.postcode.clear();
        AddCustomerPage.postcode.sendKeys(map.get("postercodeparameter"));
        Thread.sleep(2000);
    }

    public static void clickAddCustomerBtn(WebDriver driver) throws Exception {
        AddCustomerPage.addcustomerBtn.click();
        Thread.sleep(3000);
        Alert alert = driver.switchTo().alert();
        alert.accept();
        Thread.sleep(2000);
    }

    public static void verifyAddCustomerform() {
    }
}

