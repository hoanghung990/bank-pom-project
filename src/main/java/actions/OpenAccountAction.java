package actions;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import page.AddCustomerPage;
import page.OpenAccountPage;
import steps.Setup;

import java.util.HashMap;
import java.util.List;

public class OpenAccountAction {
    public static void clickAccountpage() throws InterruptedException {
        OpenAccountPage.addcustomerBtn.click();
        Thread.sleep(2000);
    }

    public static void verifyAccountExisted(WebDriver driver, String firstname, String lastname) throws Throwable {
        WebElement select = Setup.driver.findElement(By.id("userSelect"));
        List<WebElement> listOption = select.findElements(By.tagName("option"));
        Boolean checkExisted = false;
        for (WebElement e : listOption) {
            if (e.getText().equals(firstname + " " + lastname)) {
                checkExisted = true;
            }
        }
        Assert.assertEquals(checkExisted, true);
    }

    public static void selectCustomer(WebDriver driver, HashMap<String, String> map) throws Throwable {
        Select selectcustomer = new Select(Setup.driver.findElement(By.id("userSelect")));
        selectcustomer.selectByVisibleText(map.get("customernamep"));
        Thread.sleep(2000);
        Select selectcurrency = new Select(Setup.driver.findElement(By.id("currency")));
        selectcurrency.selectByVisibleText(map.get("currencyp"));
        Thread.sleep(2000);
    }

    public static void verifyOpenAccountform() {
    }

    public static void clickProcessBtn(WebDriver driver) throws Throwable {
        OpenAccountPage.processBtn.click();
        Thread.sleep(3000);
        Alert alert = driver.switchTo().alert();
        alert.accept();
        Thread.sleep(2000);
    }
}
