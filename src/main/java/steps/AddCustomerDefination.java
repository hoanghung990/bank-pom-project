package steps;

import actions.OpenAccountAction;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import actions.AddCustomerAction;
import gherkin.lexer.Th;
import org.openqa.selenium.support.PageFactory;
import page.AddCustomerPage;
import page.OpenAccountPage;

import java.util.HashMap;

public class AddCustomerDefination {
    private String firstnamestore ="";
    private String lastnamestore ="";
    @Given("^I open the website (.+)$")
    public void i_open_the_website(String website) throws Throwable {
        Setup.setupDriver();
        Setup.driver.get(website);
        Thread.sleep(3000);
    }

    @When("^I type firstname (.+), last name (.+), poster code (.+)$")
    public void i_type_firstname_last_name_poster_code(String firstname, String lastname, String postercode) throws Throwable {
        PageFactory.initElements(Setup.driver, AddCustomerPage.class);//lay tat ca element tu trang Add Customer page
        HashMap<String,String> sampleData = new HashMap<String, String>();
        sampleData.put("firstnameparameter",firstname);// luu tru gia tri cua firstname vao firstnameparameter
        sampleData.put("lastnameparameter",lastname);
        sampleData.put("postercodeparameter", postercode);
        AddCustomerAction.Execute(Setup.driver,sampleData);
        firstnamestore = firstname;
        lastnamestore = lastname;
    }

    @Then("^I see the successful dialog is appeared$")
    public void i_see_the_successful_dialog_is_appeared() throws Throwable {

    }

    @Then("^I verify that the correct Add Customer form$")
    public void i_verify_that_the_correct_add_customer_form() throws Throwable {
        AddCustomerAction.verifyAddCustomerform();

    }

    @And("^I click Add Cutomer button$")
    public void i_click_add_cutomer_button() throws Throwable {
        AddCustomerAction.clickAddCustomerBtn(Setup.driver);
    }
    @And("^I verify the customer information inserts successfully$")
    public void i_verify_the_customer_information_inserts_successfully() throws Throwable {
        PageFactory.initElements(Setup.driver, OpenAccountPage.class);
        OpenAccountAction.clickAccountpage();
        OpenAccountAction.verifyAccountExisted(Setup.driver,firstnamestore,lastnamestore);

    }
}
