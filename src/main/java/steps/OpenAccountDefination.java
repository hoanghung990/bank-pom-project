package steps;

import actions.CustomersAction;
import actions.OpenAccountAction;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import page.AddCustomerPage;
import page.CustomersPage;
import page.OpenAccountPage;

import java.util.HashMap;

public class OpenAccountDefination {
    private String firstnamestore ="";
    private String lastnamestore ="";
    @Given("^I open the Open account website (.+)$")
    public void i_open_the_open_account_website(String website) throws Throwable {
        Setup.setupDriver();
        Setup.driver.get(website);
        Thread.sleep(3000);
    }

    @When("^I select the customer name (.+) (.+), select the currency (.+)$")
    public void i_select_the_customer_name_select_the_currency(String firstname, String lastname, String currency) throws Throwable {
        PageFactory.initElements(Setup.driver, OpenAccountPage.class);
        HashMap<String, String> sampleData = new HashMap<String, String>();
        //hashmap la noi chua n gia tri dau vao
        sampleData.put("customernamep", firstname+" "+lastname);
        sampleData.put("currencyp", currency);
        OpenAccountAction.selectCustomer(Setup.driver, sampleData);
        firstnamestore = firstname;
        lastnamestore = lastname;
    }

    @Then("^I verify that the correct Open account form$")
    public void i_verify_that_the_correct_open_account_form() throws Throwable {
        OpenAccountAction.verifyOpenAccountform();
    }

    @Then("^I see the Open Acount successful dialog is appeared$")
    public void i_see_the_open_acount_successful_dialog_is_appeared() throws Throwable {
    }

    @And("^I click Process button$")
    public void i_click_process_button() throws Throwable {
        OpenAccountAction.clickProcessBtn(Setup.driver);
    }

    @And("^I verify the Account is added successfully$")
    public void i_verify_the_account_is_added_successfully() throws Throwable {
        PageFactory.initElements(Setup.driver, CustomersPage.class);
        CustomersAction.clickCustomer();
        CustomersAction.verifyAccount(Setup.driver,firstnamestore,lastnamestore);
        CustomersAction.scrollTable();
    }

}
