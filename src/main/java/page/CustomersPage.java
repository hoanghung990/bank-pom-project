package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CustomersPage extends BaseClass {
    public CustomersPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(how = How.XPATH, using = "//div[1]/div/div[2]/div/div[1]/button[3]")
    public static WebElement customersBtn;

    @FindBy(how = How.TAG_NAME, using = "table")
    public static WebElement tableAccount;
}
