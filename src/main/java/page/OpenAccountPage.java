package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class OpenAccountPage extends BaseClass{
    public OpenAccountPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(how = How.XPATH, using = "//div[1]/div/div[2]/div/div[1]/button[2]")
    public static WebElement addcustomerBtn;

    @FindBy(how = How.ID, using = "userSelect")
    public static WebElement customerNamelist;

    @FindBy(how = How.ID, using = "currency")
    public static WebElement currencylist;

    @FindBy(how = How.XPATH, using = "//div[1]/div/div[2]/div/div[2]/div/div/form/button")
    public static WebElement processBtn;
}
