package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import page.BaseClass;

public class AddCustomerPage extends BaseClass {
    public AddCustomerPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.XPATH, using = "//div[1]/div/div[2]/div/div[2]/div/div/form/div[1]/input")
    public static WebElement firstname;
    @FindBy(how = How.XPATH, using = "//div[1]/div/div[2]/div/div[2]/div/div/form/div[2]/input")
    public static WebElement lastname;
    @FindBy(how = How.XPATH, using = "//div[1]/div/div[2]/div/div[2]/div/div/form/div[3]/input")
    public static WebElement postcode;
    @FindBy(how = How.XPATH, using = "//div[1]/div/div[2]/div/div[2]/div/div/form/button")
    public static WebElement addcustomerBtn;
}

